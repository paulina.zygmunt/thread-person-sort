#include <array>
#include <iomanip>
#include "person.h"

std::ostream& operator<<(std::ostream& s,const Person & p)
{
    s   << std::left << std::setw(10) << p.get_name() 
        << std::setw(20) << p.get_surname() 
        << std::setw(5)  << p.get_age();
    return s;
}

Person Person::get_random_person()
{
    const std::array<std::string,10> names{
        "Kasia", "Tomek", "Ania", "Jan", "Iza",
        "Marcin", "Ola", "Kamil", "Gosia", "Patryk"};

    const std::array<std::string,10> surnames{
        "Kowalski", "Marianski", "Bojarczyk", "Kapusta", "Marcinkiewicz", 
        "Baran", "Koza", "Krosno", "Nalepka", "Nalewka"};

    return Person(
            names[std::rand() % (names.size())],
            surnames[std::rand() % (surnames.size())],
            std::rand()%50+20);
}
