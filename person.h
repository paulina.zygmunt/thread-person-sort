#ifndef __PERSON_H
#define __PERSON_H

#include <iostream>

class Person {
    private:
        std::string name;
        std::string surname;
        int age{0};

    public:

        Person() = default;

        Person( const std::string & _name,
                const std::string _surname,
                const int _age):
                    name(_name),surname(_surname),age(_age){};

        Person(const Person & p):
            name(p.name),surname(p.surname),age(p.age){};

        const std::string & get_name() const {
            return name;
        }

        const std::string & get_surname() const {
            return surname;
        }

        const int get_age() const {
            return age;
        }

        friend std::ostream& operator<<(std::ostream& s,const Person & p);

        //utils methods
        static Person get_random_person();
};

#endif 
