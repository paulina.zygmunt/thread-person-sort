#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include "person.h"

void by_surnames(std::vector<Person> & vec_sort_surname){
    std::sort(vec_sort_surname.begin(), vec_sort_surname.end(), [](Person p1, Person p2){
        if (p1.get_surname() != p2.get_surname())
            return (p1.get_surname() < p2.get_surname());
    });
}

void by_names(std::vector<Person> & vec_sort_name){
    std::sort(vec_sort_name.begin(), vec_sort_name.end(), [](Person p1, Person p2){
        if (p1.get_name() != p2.get_name())
            return (p1.get_name() < p2.get_name());
    });
}

void by_ages(std::vector<Person> & vec_sort_age){
    std::sort(vec_sort_age.begin(), vec_sort_age.end(), [](Person p1, Person p2){
        if (p1.get_age() != p2.get_age())
            return (p1.get_age() < p2.get_age());
    });
}

void print(std::vector<Person> vec, int k){
    for (int i=0;i<k;++i)
        std::cout << vec.at(i) << std::endl;
    std::cout << std::endl;
}


int main(int, char**)
{
//1
    std::srand(0);
    std::vector<Person> vec_all;
    for (int i=0;i<1000;++i)
        vec_all.push_back(Person::get_random_person());
//2
    std::vector<Person> vec_sort_surname = vec_all;
    std::vector<Person> vec_sort_name = vec_all;
    std::vector<Person> vec_sort_age = vec_all;
//3
    std::thread first (by_surnames, std::ref(vec_sort_surname));
    std::thread second (by_names, std::ref(vec_sort_name));
    std::thread third (by_ages, std::ref(vec_sort_age));

    first.join();
    second.join();
    third.join();
//4
    print(vec_sort_surname, 10);
    print(vec_sort_name, 10);
    print(vec_sort_age, 10);

    return 0;
}
